import com.filenet.api.core.Connection;
import com.filenet.api.core.Factory;
import com.filenet.api.util.UserContext;
import filenet.vw.api.VWQueue;
import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWSession;
import filenet.vw.api.VWWorkObject;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.security.auth.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class main {
    public static Logger logger = LoggerFactory.getLogger(main.class);
    public static Properties prop = new Properties();
    public static VWSession vwSession;
    public static VWQueue conductor;

    public static void main(String[] args) {
        logger.info("started");
        logger.trace("loading properties");
        InputStream pfile = main.class.getClassLoader().getResourceAsStream("config.prop");
        try {
            prop.load(pfile);
        } catch (IOException e) {
            logger.error("in exception");
            e.printStackTrace();
        }
        logger.debug("loading server");
        String server = prop.getProperty("server");
        logger.trace(server);
        String port = prop.getProperty("port");
        logger.trace(port);
        String username = prop.getProperty("username");
        logger.trace(username);
        String password = prop.getProperty("password");
        logger.trace(password);
        String cp = prop.getProperty("cp");
        logger.trace(cp);
        String os = prop.getProperty("os");
        logger.trace(os);
        Connection con = Factory.Connection.getConnection("http://" + server + ":" + port + "/wsi/FNCEWS40MTOM");
        Subject subject = UserContext.createSubject(con, username, password, "FileNetP8WSI");
        UserContext uc = UserContext.get();
        uc.pushSubject(subject);
        vwSession = new VWSession(username, password, cp);
        vwSession.logon(username, password, cp);
        conductor = vwSession.getQueue("Conductor");
        logger.debug("total review count:" + conductor.fetchCount());
        VWQueueQuery vwQwery = conductor.createQuery(null, null, null, 1, null, null, 1);
        while (vwQwery.hasNext()) {
            VWWorkObject vwWorkObject = (VWWorkObject) vwQwery.next();
            String pn = vwWorkObject.getWorkflowName();
            logger.info("\n");
            logger.info(vwWorkObject.getWorkflowNumber());
            logger.info("error: " + vwWorkObject.getLastErrorNumber() + " with text: " + vwWorkObject.getLastErrorText());
            if (pn.toLowerCase().contains("mail")) {
                try {
                    vwWorkObject.doLock(Boolean.TRUE.booleanValue());
                    vwWorkObject.doTerminate();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                    e.printStackTrace();
                }
            } else {
                if (pn.toLowerCase().contains("archive")) {
                    logger.info("ArchiveProcess. Skipping");
                } else {
                    try {
                        vwWorkObject.doLock(Boolean.TRUE.booleanValue());
                        vwWorkObject.setSelectedResponse("Повторить");
                        vwWorkObject.doDispatch();
                    } catch (Exception e) {
                        e.printStackTrace();
                        logger.error(e.getMessage());
                    }
                }
            }
        }
    }
}
